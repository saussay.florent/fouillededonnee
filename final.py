import numpy as np
import keras_preprocessing.image as Image
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPool2D, MaxPooling2D
from scipy.sparse.construct import random
from sklearn.model_selection import train_test_split
import os
import tensorflow as tf
from tensorflow import keras
from matplotlib import pyplot as plt
import random

address = './archive/'
BATCH_SIZE = 120  # Size of the trainin data batch used
EPOCHS = 40


def get_all_files():
    # This function returns a list of all dataset file names
    all_files = []
    directory_names = os.listdir(address)

    for name in directory_names:
        try:
            if (os.path.isdir(address + name + "/1/")):
                #print("bob1")
                files = os.listdir(address + name + "/1/")
                for file_name in files:
                    all_files.append(address + name + "/1/" + file_name)
            if (os.path.isdir(address + name + "/0/")):
                #print("BBOOOOO")
                files = os.listdir(address + name + "/0/")
                for file_name in files:
                    all_files.append(address + name + "/0/" + file_name)
        except:
            FileNotFoundError

    return all_files


def separate_image_labels(files):
    # This function creates two parallel arrays; Image and Labels
    labels = []
    images = []
    for file in files:
        try:
            image = tf.cast(
                Image.img_to_array(Image.load_img(file, target_size=(50, 50))),
                tf.float32) / 256.0
            labels.append(int(file.split("/")[-2]))
            images.append(image)
        except:
            FileNotFoundError

    print(len(labels))
    print(len(images))
    return (labels, images)


def split_train_test(labels, images):
    y = np.array(labels)
    x = np.stack(images)

    x_train, x_test, y_train, y_test = train_test_split(x,
                                                        y,
                                                        test_size=0.4,
                                                        random_state=42)
    y_train = tf.keras.utils.to_categorical(y_train)
    y_test = tf.keras.utils.to_categorical(y_test)
    print(x_train.shape, x_test.shape, y_train.shape, y_test.shape)
    return (x_train, x_test, y_train, y_test)


def setModel():
    """
    This function sets layers of our neural network ( .Sequential)
    Then compile them with some parameters( .compile)
    Return a tf.keras object as a model
    """
    model = tf.keras.Sequential()
    model.add(
        Conv2D(filters=32,
               kernel_size=(4, 4),
               input_shape=(50, 50, 3),
               activation='relu'))
    model.add(Conv2D(filters=32, kernel_size=(4, 4), activation='relu'))
    model.add(MaxPool2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dense(2, activation='sigmoid'))

    model.compile(loss='binary_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])

    model.summary()
    return model


def trainModel(model, x_train, y_train):
    """
    Function that trains our neural network ( .fit)
    Input :
        - training_dataset : the training dataset
        - validation_datatset : the validation dataset
    """
    callback = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=3)
    history = model.fit(
        x_train,
        y_train,
        epochs=EPOCHS,
        verbose=1,
        validation_split=
        0.2,  #save 20% of the training dataset for validation and validate
        batch_size=BATCH_SIZE,
        callbacks=[callback])
    return history


def evaluateModel(x_test, y_test):
    print("Evaluate on test data")
    results = model.evaluate(x_test, y_test, batch_size=BATCH_SIZE)
    print("test loss, test acc:", results)


def showAccuracyGraph(history):
    plt.subplot(1, 2, 1)
    plt.plot(history.history['accuracy'])
    plt.plot(history.history['val_accuracy'])
    plt.title('Model Accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'validation'], loc='upper left')


def showLossGraph(history):
    plt.subplot(1, 2, 2)
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.legend(['train', 'val'], loc='upper left')


if __name__ == '__main__':
    all_files = get_all_files()
    random.seed(42)
    random.shuffle(all_files)
    all_files = all_files[:20000]
    (labels, images) = separate_image_labels(all_files)
    (x_train, x_test, y_train, y_test) = split_train_test(labels, images)
    model = setModel()
    history = trainModel(model, x_train, y_train)
    evaluateModel(x_test, y_test)
    showAccuracyGraph(history)
    showLossGraph(history)
    plt.show()
